# v1.1.4

- fix: use correct WM startup class (The one with the braid)

# v1.1.3

- chore: bump pkpass (The one with the braid)
- chore: decrease size of screenshots (The one with the braid)
- chore: format metadata (The one with the braid)
- Created spanish translation (Diego)
- feat: add badges to README (The one with the braid)
- fix: add fallback background colors to passes (The one with the braid)
- fix: Debian icon names (The one with the braid)
- fix: debian launcher path (The one with the braid)
- fix: encode PkPass URI components (The one with the braid)
- fix: icon symbolic link (The one with the braid)

# v1.1.2

- chore: bump flutter (The one with the braid)
- chore: use custom Docker images (The one with the braid)
- chore: use platform-specific docker images (The one with the braid)
- feat: support cache for update checks (The one with the braid)
- fix: add missing Android network permission (The one with the braid)

# v1.1.1

- feat: add basic web service support (The one with the braid)
- fix: invalid filter on HTML content (The one with the braid)
- fix: use stable Debian on aarch64 (The one with the braid)

# v1.1.0

- chore: add Debian control files (The one with the braid)
- chore: add aarch64 Linux builds (The one with the braid)
- chore: add about dialog (The one with the braid)
- chore: improve README (The one with the braid)
- feat: add linter CI job (The one with the braid)
- feat: apply strict code style (The one with the braid)
- feat: implement PkPass share (The one with the braid)
- feat: support dynamic colors (The one with the braid)
- fix: Hero around Scaffold (The one with the braid)
- fix: add fallback colors for label field (The one with the braid)
- fix: enable multidex (The one with the braid)
- fix: escaped line breaks in PkPass files (The one with the braid)
- fix: supress intent error on desktops (The one with the braid)

# v1.0.6

- feat: make APK build reproducible

# v1.0.5

- feat: make APK build reproducible

# v1.0.4

- feat: make APK build reproducible

# v1.0.3

- feat: make APK build reproducible

# v1.0.2

- feat: pin Flutter version to file (The one with the braid)

# v1.0.1

- chore: add F-Droid metadata (The one with the braid)
- chore: add funding (The one with the braid)
- chore: add screenshots (The one with the braid)
- fix: typo in README (The one with the braid)
- fix: web deploy path (The one with the braid)
- fix: web deploy path (The one with the braid)

# v1.0.0

- chore: add CI (The one with the braid)
- chore: implement passs deletion (The one with the braid)
- chore: support DPI related scaling for PkPass assets (The one with the braid)
- chore: update README and add LICENSE (The one with the braid)
- CI: fix Android bundle path (The one with the braid)
- CI: fix Android bundle (The one with the braid)
- CI: fix Android keystore location (The one with the braid)
- CI: fix flutter-gen (The one with the braid)
- CI: fix Linux arch (The one with the braid)
- feat: add Linux cli support (The one with the braid)
- feat: add Linux desktop file (The one with the braid)
- feat: filter passes by category (The one with the braid)
- feat: simplify Android intents (The one with the braid)
- feat: sort passes by date (The one with the braid)
- feat: support expiry date time (The one with the braid)
- feat: support link open (The one with the braid)
- feat: support receiving sharing intent on Android (The one with the braid)
- feat: use system theme for splash screen (The one with the braid)
- fix: Android intent filter (The one with the braid)
- fix: Android intent single file (The one with the braid)
- fix: Android key decoding (The one with the braid)
- fix: file filter on Android (The one with the braid)
- fix: flicking (The one with the braid)
- fix: max width of logos in preview (The one with the braid)
- fix: pass deletion confirmation (The one with the braid)
