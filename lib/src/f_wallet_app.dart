import 'package:flutter/material.dart';

import 'package:dynamic_color/dynamic_color.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';

import 'package:f_wallet/src/utils/f_wallet_theme.dart';
import 'package:f_wallet/src/utils/hive_boxes.dart';
import 'package:f_wallet/src/utils/pass_manager.dart';
import 'package:f_wallet/src/utils/router.dart';
import 'package:f_wallet/src/widgets/theme_button.dart';

class FWalletApp extends StatelessWidget {
  static final _appKey = GlobalKey();

  const FWalletApp({super.key});

  @override
  Widget build(BuildContext context) {
    return InheritedProvider<PassManager>(
      create: (context) => PassManager.instance(),
      child: ValueListenableBuilder(
        builder: (context, box, child) => DynamicColorBuilder(
          builder: (light, dark) {
            return MaterialApp.router(
              key: _appKey,
              onGenerateTitle: (context) =>
                  AppLocalizations.of(context)?.appName ?? 'fWallet',
              themeMode: ThemeButton.getThemeMode(box),
              theme: FWalletTheme.light(light),
              darkTheme: FWalletTheme.dark(dark),
              routerConfig: fWalletRouter,
              locale: getLocale(box),
              localizationsDelegates: const [
                GlobalWidgetsLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                ...AppLocalizations.localizationsDelegates,
              ],
              supportedLocales: AppLocalizations.supportedLocales,
            );
          },
        ),
        valueListenable: HiveBoxes.settings.listenable(),
      ),
    );
  }

  Locale? getLocale(Box<Object?> box) {
    final localeName = box.get('locale') as String?;
    return AppLocalizations.supportedLocales
        .where(
          (supportedLocale) => supportedLocale.languageCode == localeName,
        )
        .singleOrNull;
  }
}
