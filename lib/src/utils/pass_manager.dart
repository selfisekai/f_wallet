import 'dart:developer';

import 'package:flutter/foundation.dart';

import 'package:hive_flutter/hive_flutter.dart';
import 'package:pkpass/pkpass.dart';
import 'package:pkpass/pkpass_web_service.dart';

import 'package:f_wallet/src/utils/hive_boxes.dart';
import 'cached_web_service.dart';

class PassManager extends ValueListenable<PassManager> {
  static PassManager? _instance;

  static LazyBox<Uint8List> get box => HiveBoxes.passes;

  Map<String, PassFile> passFiles;

  PassManager(Map<String, PassFile> files) : passFiles = files {
    _updatePasses();
  }

  factory PassManager.instance() {
    assert(
      _instance != null,
      'PassManager was not initialized yet. Please await PassManager.init first.',
    );
    return _instance!;
  }

  static Future<void> init() async {
    final buffers = (await Future.wait(
      List.generate(box.length, (index) => box.getAt(index)),
    ))
        .whereType<Uint8List>();
    final passes = await Future.wait(
      buffers.map((e) => _tryParse(e)),
    );

    _instance = PassManager(
      Map.fromEntries(
        passes
            .whereType<PassFile>()
            .map((e) => MapEntry(e.metadata.serialNumber, e)),
      ),
    );
  }

  static Future<PassFile?> _tryParse(Uint8List file) async {
    try {
      return await PassFile.parse(file);
    } catch (e, s) {
      log(
        'Error parsing pass file.',
        error: e,
        stackTrace: s,
        name: 'PassManager',
      );
      return null;
    }
  }

  void _updatePasses() {
    for (final pass in passFiles.keys) {
      checkForUpdate(pass);
    }
  }

  Future<String?> save(Uint8List file) async {
    final pass = await _tryParse(file);
    if (pass == null) return null;

    final serialNumber = pass.metadata.serialNumber;
    passFiles[serialNumber] = pass;
    await box.put(serialNumber, file);
    return serialNumber;
  }

  Future<void> delete(String serialNumber) async {
    await box.delete(serialNumber);
    passFiles.remove(serialNumber);
  }

  Future<bool?> checkForUpdate(String serialNumber) async {
    final pass = passFiles[serialNumber];
    if (pass == null) return null;

    final webservice = CachedWebService(pass.metadata);
    Uint8List? result;
    try {
      result = await webservice.getLatestVersion();
    } on PkPassWebServiceError {
      log(
        'Server error during update check for ${pass.metadata.passTypeIdentifier}: $serialNumber.',
        name: 'PassManager',
      );
      return null;
    }
    if (result == null) return false;

    String? serial;
    try {
      serial = await save(result);
      if (serial == null) throw InvalidEncodingError();
    } on PKPassError {
      log(
        'Updated pass file for ${pass.metadata.passTypeIdentifier}: $serialNumber is not a valid PkPass file. Discarding.',
        name: 'PassManager',
      );
      return null;
    }
    log(
      'Got update for pass ${pass.metadata.passTypeIdentifier}: $serialNumber ...',
      name: 'PassManager',
    );
    return true;
  }

  @override
  void addListener(VoidCallback listener) =>
      box.listenable().addListener(listener);

  @override
  void removeListener(VoidCallback listener) =>
      box.listenable().removeListener(listener);

  @override
  PassManager get value => this;
}
