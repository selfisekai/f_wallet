import 'dart:typed_data';

import 'package:hive_flutter/hive_flutter.dart';

import 'package:f_wallet/src/utils/secure_storage.dart';

class HiveBoxes {
  const HiveBoxes();

  static const _settingsBoxName = 'settings';
  static const _passesBoxName = 'passes';
  static const _webServiceBoxName = 'webservice';

  static Future<void> init() async {
    HiveCipher? cipher;
    final key = await SecureStorage.getHiveCipher();
    if (key != null) cipher = HiveAesCipher(key);

    await Hive.initFlutter();
    await Future.wait(
      [
        Hive.openBox<Object?>(_settingsBoxName, encryptionCipher: cipher),
        Hive.openBox<DateTime>(_webServiceBoxName, encryptionCipher: cipher),
        Hive.openLazyBox<Uint8List>(_passesBoxName, encryptionCipher: cipher),
      ],
    );
  }

  static Box<Object?> get settings => Hive.box<Object?>(_settingsBoxName);

  static Box<DateTime> get webService => Hive.box<DateTime>(_webServiceBoxName);

  static LazyBox<Uint8List> get passes =>
      Hive.lazyBox<Uint8List>(_passesBoxName);
}
