import 'package:flutter/material.dart';

import 'package:dynamic_color/dynamic_color.dart';

class FWalletTheme {
  static ThemeData light([ColorScheme? light]) => ThemeData.from(
        colorScheme: light?.harmonized() ??
            ColorScheme.fromSeed(
              seedColor: Colors.pink,
              brightness: Brightness.light,
            ),
        useMaterial3: true,
      );

  static ThemeData dark([ColorScheme? dark]) => ThemeData.from(
        colorScheme: dark?.harmonized() ??
            ColorScheme.fromSeed(
              seedColor: Colors.deepPurple,
              brightness: Brightness.dark,
            ),
        useMaterial3: true,
      );
}
