import 'dart:convert';
import 'dart:developer';
import 'dart:math' hide log;

import 'package:flutter/foundation.dart';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureStorage {
  static FlutterSecureStorage? _storage;

  static const _jsonCodec = JsonCodec();

  static const _hiveKey = 'hive_cipher';
  static const _storageKey = 'storage_cipher';

  static final _random = Random.secure();

  static Future<void> init() async {
    try {
      // ignore: unnecessary_new, prefer_const_constructors
      final storage = new FlutterSecureStorage(
        aOptions: const AndroidOptions(encryptedSharedPreferences: true),
      );

      if (!await storage.containsKey(key: _hiveKey)) {
        final key = List.generate(32, (index) => _random.nextInt(255));
        await storage.write(key: _hiveKey, value: _jsonCodec.encode(key));
      }
      if (!await storage.containsKey(key: _storageKey)) {
        final key = List.generate(32, (index) => _random.nextInt(255));
        await storage.write(key: _storageKey, value: _jsonCodec.encode(key));
      }

      _storage = storage;
    } catch (e, s) {
      log(
        'Error initializing SecureStorage.',
        name: 'SecureStorage',
        error: e,
        stackTrace: s,
      );
    }
  }

  static Future<Uint8List?> _getUtint8List(String name) async {
    final cipherString = await _storage?.read(key: name);
    if (cipherString == null) return null;
    final list = jsonDecode(cipherString) as List<dynamic>;
    return Uint8List.fromList(
      list.cast<int>(),
    );
  }

  static Future<Uint8List?> getHiveCipher() => _getUtint8List(_hiveKey);

  static Future<Uint8List?> getStorageCipher() => _getUtint8List(_storageKey);
}
