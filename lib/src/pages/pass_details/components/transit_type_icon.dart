import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:pkpass/pkpass.dart';

class TransitTypeIcon extends StatelessWidget {
  final TransitType? transitType;
  final DateTime? relevantDate;
  final DateTime? expirationDate;

  const TransitTypeIcon({
    super.key,
    required this.transitType,
    this.relevantDate,
    this.expirationDate,
  });

  @override
  Widget build(BuildContext context) {
    if (this.relevantDate == null && transitType == null) return Container();

    Widget? icon;

    switch (transitType) {
      case TransitType.air:
        icon = const Icon(Icons.airplane_ticket);
      case TransitType.boat:
        icon = const Icon(Icons.directions_boat);
      case TransitType.bus:
        icon = const Icon(Icons.directions_bus);
      case TransitType.train:
        icon = const Icon(Icons.directions_railway);
      default:
        icon = null;
    }

    String title;
    final relevantDate = this.relevantDate;
    if (relevantDate != null) {
      // show date only if no time of the day specified

      if (relevantDate.hour == 0 && relevantDate.minute == 0) {
        title = AppLocalizations.of(context)!.yourPassDate(relevantDate);
      }
      // if time of the day specified, show full datetime
      else {
        title = AppLocalizations.of(context)!
            .yourPassDateTime(relevantDate, relevantDate);
      }
    } else {
      title = AppLocalizations.of(context)!.yourPass;
    }

    String? subtitle;
    final expirationDate = this.expirationDate;
    if (expirationDate != null) {
      // show date only if no time of the day specified

      if (expirationDate.hour == 0 && expirationDate.minute == 0) {
        subtitle = AppLocalizations.of(context)!.endingDate(expirationDate);
      }
      // if time of the day specified, show full datetime
      else {
        subtitle = AppLocalizations.of(context)!
            .endingDateTime(expirationDate, expirationDate);
      }
    }

    return ListTile(
      leading: icon,
      title: Text(title),
      subtitle: subtitle != null ? Text(subtitle) : null,
    );
  }
}
