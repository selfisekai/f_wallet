# ![fWallet logo](assets/logo/logo-circle.svg) fWallet

[![F-Droid](https://img.shields.io/f-droid/v/business.braid.f_wallet?style=for-the-badge&logo=fdroid&label=F-Droid)](https://f-droid.org/packages/business.braid.f_wallet/)
[![GitLab tag](https://img.shields.io/gitlab/v/tag/TheOneWithTheBraid%2Ff_wallet?style=for-the-badge&logo=gitlab)](https://gitlab.com/TheOneWithTheBraid/f_wallet/-/tags/)
[![Gitlab pipeline status (self-managed)](https://img.shields.io/gitlab/pipeline-status/TheOneWithTheBraid%2Ff_wallet?style=for-the-badge)](https://gitlab.com/TheOneWithTheBraid/f_wallet/-/pipelines)

A beautiful cross-platform wallet application for your transport tickets, discount cards and subscriptions.

Written in Dart using the [package:pkpass](https://pub.dev/packages/pkpass) pkpass Dart library.

---

**Now available: Linux aarch64 builds eligible for Linux on mobile !**

## Features

- easily import PkPass files and show them on purpose
- beautifully designed
- no trackers, no Google, no proprietary dependencies

## Screenshots

|                                    Mobile dark                                     |                                     Mobile light                                     |                                                                                    Desktop                                                                                    |
|:----------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| ![fWallet on mobile in dark mode](assets/screenshots/mobile/dark.png){height=175%} | ![fWallet on mobile in light mode](assets/screenshots/mobile/light.png){height=175%} | ![fWallet on desktop in dark mode](assets/screenshots/desktop/dark.png){width=50%} <br/> ![fWallet on desktop in light mode](assets/screenshots/desktop/light.png){width=50%} |

## Downloads

There are debug builds for each merge request and production builds for each revision on `main` branch as well as tags.

The current `main` is being deployed for web use at https://theonewiththebraid.gitlab.io/f_wallet/ .

Download here : https://gitlab.com/TheOneWithTheBraid/f_wallet/-/tags

## Getting Started

Compile it yourself :

```shell
dart pub get
flutter gen-l10n
flutter build linux
```

Import a file via CLI (GUI is boring) :

```shell
./f_wallet /path/to/pass.pkpass /maybe/even/a/second/file.pkpass
```

## Supported platforms

- web
- Linux
    - aarch64
    - amd64
- Android (combined build)
    - armeabi-v7a, arm64-v8a, x86, x86_64
    - KitKat 4.4 / API level 19 at minimum

## License

Like this project? [Buy me a Coffee](https://www.buymeacoffee.com/braid).

This project is [EUPL-1.2](LICENSE) licensed.